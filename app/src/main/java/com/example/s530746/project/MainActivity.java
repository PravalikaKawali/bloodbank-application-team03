package com.example.s530746.project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText mEmail,mPassword;
    String email,passw;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.blood);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_main);

        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        Button signUp = (Button) findViewById(R.id.signUp);


        final Context context = this;

        signUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg) {
                Intent viewIntent = new Intent(context, signUp.class);
                startActivity(viewIntent);
            }
        });



        final Button email_sign_in_button = (Button) findViewById(R.id.email_sign_in_button);

        email_sign_in_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg) {
                email = mEmail.getText().toString();
                passw = mPassword.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Required");
                }
                if (TextUtils.isEmpty(passw)) {
                    mPassword.setError("Required");

                }

                else if(!isValidEmailAddress(email)){

                    mEmail.setError("Invalid");
                }

                else if(!isValidPassword(passw)){

                    mPassword.setError("Invalid");
                }

                else {
                    Intent viewIntent = new Intent(context, BasicActivity.class);
                    startActivity(viewIntent);
                }


            }
        });
    }


    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public boolean isValidPassword(String passw) {
        String ePattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(passw);
        return m.matches();
    }

    }


