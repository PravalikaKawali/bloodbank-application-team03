package com.example.s530746.project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class signUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.blood);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_sign_up);
        final Context context = this;

        Button btn_link_login = (Button) findViewById(R.id.btn_link_login);
        btn_link_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent viewIntent = new Intent(context, MainActivity.class);
                startActivity(viewIntent);

            }
        });

    }
}
