package com.example.s530746.project;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BasicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.blood);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        setContentView(R.layout.activity_basic);
        final Context context = this;

        Button create_uni_details = (Button) findViewById(R.id.btnInfo);

        create_uni_details.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg) {
                Intent viewIntent = new Intent(context, info_activity.class);
                startActivity(viewIntent);
            }
        });
    }
    public void clickDonor(View v)
    {
        Button btn1=(Button)findViewById(R.id.btnDonor);
        Intent intent=new Intent(this,DonorActivity.class);
        startActivity(intent);
    }
    public void clickNeed(View v)
    {
        Button btn2=(Button)findViewById(R.id.btnNeed);
        Intent ini=new Intent(this,UserActivity.class);
        startActivity(ini);
    }
}
