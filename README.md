APP name - Blood Bank
Team Number - 03 Course Number - 44-644

1.Project Members:
Nagateja Seetharama Srinivas Prakash Pakala(s530715@nwmissouri.edu) Course: MS-ACS Semester:02

Pravalika Kawali(s530936@nwmissouri.ed) Course: MS-ACS Semester:02

Sreelakshmi Onteddu(s530746@nwmissouri.edu) Course: MS-ACS Semester:02

2.Link to the public repository:
https://bitbucket.org/PravalikaKawali/bloodbank-application-team03

3.Supported Devices:
Devices with API 19 or above

4.App developed in IDE:
Android Studio

5.Description:
The android app helps people who need blood during emergency situations and also show people on map who are wiling to donote blood. You have the options to provide Your name, city, your blood group and a mobile number to contact with the donor. If you want to donate blood to anyone who is in your city can find you easily by using this app

6.Test Credentials: Users should login into the app with the help of username and password. The Username should be Emailid and password should have atleast 8 characters(A-Z, a-z, 0-9, special characters including(!,@,#,$,%,^,&,*)).